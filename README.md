# 掘金自动签到程序

> 每日两次，防止错过

## 复制掘金的cookie信息
可以使用谷歌插件editThisCookie，复制出cookie后导入到echone.json

记得把cookie中sameSite改成None，示例给的是假的哈

## 微信通知

```shell script
https://sct.ftqq.com/

https://sctapi.ftqq.com/****************.send?title=messagetitle&desp=messagecontent
```

## 配置参数

linux中部署的时候需要将 app中的headless改为True， window运行则改为False

修改app中的 SERVER_KEY参数为你自己KEY


**程序启动后会默认检测一次今日是否签到，然后每12小时检测一次，防止没有签到成功**


## docker部署

```
docker build -t juejin .

docker run -itd --restart=always --name juejin juejin
```