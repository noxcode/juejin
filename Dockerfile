FROM mcr.microsoft.com/playwright:focal
COPY . /app
WORKDIR /app
RUN pip install -r requirements.txt -i https://mirrors.aliyun.com/pypi/simple/
python -m playwright install
CMD [ "python","app.py" ]